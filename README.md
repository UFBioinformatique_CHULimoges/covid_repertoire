# Code for iScience manuscript ISCIENCE-D-22-03671R1 manuscript

`changeoToClonotypes.py` : script to convert the changeO clonotype format file to our clonotypes format file
`findcovabdab.py`: find clonotypes shared between oru repertoires and the Covabdab database
`write_clonotypes.py`: search commons clonotypes between our repertoires, and write a file with all the clonotypes.
`heatmap_ms.R` : R script to plot the VJ heatmap for ARDS/HV cohorts